# Terraform Cloud Run Module

This module handles the basic deployment of containerized applications on Cloud Run IAM role for the service.

The resources that this module will create are:

* Creates a Cloud Run service with provided name and container
* Applies Cloud Run Invoker role to members

## Prerequisites

This module assumes that below mentioned prerequisites are in place before consuming the module.

* All required APIs are enabled in the GCP Project
* Environment Variables in Secret Manager (optional)

## Requirements

These sections describe requirements for using this module.

### Software
- Terraform 0.14 or higher
- `google` provider 3.67.0 or higher.
- `google-beta` provider 3.67.0 or higher.

## Usage

Basic usage of this module is as follows:

```hcl
module "cloudrun" {
  source     = "../modules/cloudrun"

  # Required variables
  service_name           = "<Cloud run service name>"
  project_id             = "<project_id>"
  location               = "<location>" 
  image                  = "gcr.io/cloudrun/hello" # public container image
  
  #optional variable
  ingress				         = "internal-and-cloud-load-balancing"  
  

  
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| argument | Arguments passed to the ENTRYPOINT command, include these only if image entrypoint needs arguments | `list(string)` | `[]` | no |
| container\_command | Leave blank to use the ENTRYPOINT command defined in the container image, include these only if image entrypoint should be overwritten | `list(string)` | `[]` | no |
| container\_concurrency | Concurrent request limits to the service | `number` | `null` | no |
| env\_secret\_vars | [Beta] Environment variables (Secret Manager) | <pre>list(object({<br>    name = string<br>    value_from = set(object({<br>      secret_key_ref = map(string)<br>    }))<br>  }))</pre> | `[]` | no |
| env\_vars | Environment variables (cleartext) | <pre>list(object({<br>    value = string<br>    name  = string<br>  }))</pre> | `[]` | no |
| generate\_revision\_name | Option to enable revision name generation | `bool` | `true` | no |
| image | GCR hosted image URL to deploy | `string` | n/a | yes |
| ingress | Annotations to the service. Acceptable values all, internal, internal-and-cloud-load-balancing | `map(string)` | <pre>{<br>  "run.googleapis.com/ingress": "all"<br>}</pre> | no |
| labels | A set of key/value label pairs to assign to the service | `map(string)` | `{}` | no |
| limits | Resource limits to the container | `map(string)` | `null` | no |
| location | Cloud Run service deployment location | `string` | n/a | yes |
| members | Users/SAs to be given invoker access to the service | `list(string)` | `[]` | no |
| ports | Port which the container listens to (http1 or h2c) | <pre>object({<br>    name = string<br>    port = number<br>  })</pre> | <pre>{<br>  "name": "http1",<br>  "port": 8080<br>}</pre> | no |
| project\_id | The project ID to deploy to | `string` | n/a | yes |
| requests | Resource requests to the container | `map(string)` | `{}` | no |
| service\_account\_email | Service Account email needed for the service | `string` | `""` | no |
| service\_name | The name of the Cloud Run service to create | `string` | n/a | yes |
| template\_annotations | Annotations to the container metadata including VPC Connector and SQL. See [more details](https://cloud.google.com/run/docs/reference/rpc/google.cloud.run.v1#revisiontemplate) | `map(string)` | <pre>{<br>  "autoscaling.knative.dev/maxScale": 2,<br>  "autoscaling.knative.dev/minScale": 1,<br>  "generated-by": "terraform",<br>  "run.googleapis.com/client-name": "terraform"<br>}</pre> | no |
| template\_labels | A set of key/value label pairs to assign to the container metadata | `map(string)` | `{}` | no |
| timeout\_seconds | Timeout for each request | `number` | `120` | no |
| traffic\_split | Managing traffic routing to the service | <pre>list(object({<br>    latest_revision = bool<br>    percent         = number<br>    revision_name   = string<br>  }))</pre> | <pre>[<br>  {<br>    "latest_revision": true,<br>    "percent": 100,<br>    "revision_name": "v1-0-0"<br>  }<br>]</pre> | no |
| volume\_mounts | [Beta] Volume Mounts to be attached to the container (when using secret) | <pre>list(object({<br>    mount_path = string<br>    name       = string<br>  }))</pre> | `[]` | no |
| volumes | [Beta] Volumes needed for environment variables (when using secret) | <pre>list(object({<br>    name = string<br>    secret = set(object({<br>      secret_name = string<br>      items       = map(string)<br>    }))<br>  }))</pre> | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| service\_name | Name of the created service |
| service\_url | The URL on which the deployed service is available |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Service Account

A service account can be used with required roles to execute this module:

- Cloud Run invoker   :  `roles/run.invoker`
  * Can invoke services
- Cloud Run admin     :  `roles/run.admin`
  * Can create, update, and delete services.
  * Can get and set IAM policies.
  * Can view, apply and dismiss recommendations.
  * Requires additional configuration in order to deploy services.
- Cloud Run Developer :`roles/run.developer`
  * Can create, update, and delete services.
  * Can get but not set IAM policies.
  * Can view, apply and dismiss recommendations.  


### APIs

A project with the following APIs enabled must be used to host the main resource of this module:

- Google Cloud Run: `run.googleapis.com`
- Serverless VPC Access (optional): `vpcaccess.googleapis.com`

