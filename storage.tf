resource "google_storage_bucket" "bucket" {
#Number of bucket
 count = 1
#Bucket name
 name = "bucket1-store-tfile"
 labels = {
    key = "test" 
    value = "store"
  }
# Any location of your choice
 location = "europe-west2"
# Any storage_class of your choice
 storage_class = "STANDARD"
 uniform_bucket_level_access = true
}
