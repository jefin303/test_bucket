# service-project

A Terraform plan for deploying all service-project resources. Please refer to the following documentation for an explanation about a shared vpc in GCP: [shared vpc documentation](https://cloud.google.com/vpc/docs/shared-vpc)

## Background

This root terraform is designed to build all service-project components, which includes connecting to the shared vpc via connector. This should include serverless vpc connector, iot core, cloud functions, cloud run, and cloud pub-sub).

## Implementation

Change the module and variable values required for the module in the `main.tf` file. the following steps:

1. Create a `dev-service.tfvars` file containing the variables defined in `variables.tf` and their values. Each region will have its own tfvars file.

```hcl
credentials_file  = "~/.gcloud/ict.json"
billing_account   = "00615A-599C36-C77931"
folder_id         = "folders/235483870619"
project_base_name = "ict-sp"
environment       = "dev"
assign_unique_id  = true
unique_id         = "5f46"
host_project_id   = "dev-ict-hp-5f46"
```

2. Initialize the Terraform provider.

```bash
$ terraform init -backend-config=credentials=~/.gcloud/ict.json -backend-config=prefix=root/dev-service-project
```

3. Validate the Terraform plan.

```bash
$ terraform plan -var-file="dev-service.tfvars" -out dev-service.tfplan
```

4. Implement the Terraform plan.

```bash
$ terraform apply -auto-approve dev-service.tfplan
```

Example output:

```bash
google_project.service_project: Creating...
google_project.service_project: Still creating... [10s elapsed]
google_project.service_project: Still creating... [20s elapsed]
google_project.service_project: Creation complete after 27s [id=projects/dev-ict-sp-6ff6]
google_project_service.this["logging.googleapis.com"]: Creating...
google_project_service.this["cloudfunctions.googleapis.com"]: Creating...
google_project_service.this["container.googleapis.com"]: Creating...
google_project_service.this["compute.googleapis.com"]: Creating...
google_project_service.this["servicenetworking.googleapis.com"]: Creating...
google_project_service.this["run.googleapis.com"]: Creating...
google_project_service.this["serviceusage.googleapis.com"]: Creating...
google_project_service.this["cloudiot.googleapis.com"]: Creating...
google_project_service.this["runtimeconfig.googleapis.com"]: Creating...
google_project_service.this["cloudbuild.googleapis.com"]: Creating...
google_project_service.this["compute.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [10s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["compute.googleapis.com"]: Still creating... [20s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["compute.googleapis.com"]: Still creating... [30s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["compute.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [40s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["compute.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [50s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["compute.googleapis.com"]: Still creating... [1m0s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["compute.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [1m10s elapsed]
google_project_service.this["serviceusage.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["cloudiot.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["container.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["run.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["servicenetworking.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["logging.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["runtimeconfig.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["compute.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["cloudbuild.googleapis.com"]: Still creating... [1m20s elapsed]
google_project_service.this["cloudfunctions.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/cloudfunctions.googleapis.com]
google_project_service.this["logging.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/logging.googleapis.com]
google_project_service.this["cloudiot.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/cloudiot.googleapis.com]
google_project_service.this["serviceusage.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/serviceusage.googleapis.com]
google_project_service.this["run.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/run.googleapis.com]
google_project_service.this["runtimeconfig.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/runtimeconfig.googleapis.com]
google_project_service.this["cloudbuild.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/cloudbuild.googleapis.com]
google_project_service.this["compute.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/compute.googleapis.com]
google_project_service.this["servicenetworking.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/servicenetworking.googleapis.com]
google_project_service.this["container.googleapis.com"]: Creation complete after 1m28s [id=dev-ict-sp-6ff6/container.googleapis.com]
time_sleep.wait_api_activation: Creating...
google_runtimeconfig_config.service_project: Creating...
google_runtimeconfig_config.service_project: Creation complete after 1s [id=projects/dev-ict-sp-6ff6/configs/service-project-config]
google_runtimeconfig_variable.label_createdon: Creating...
google_runtimeconfig_variable.random_id: Creating...
google_runtimeconfig_variable.resource_labels: Creating...
google_runtimeconfig_variable.project_id: Creating...
google_runtimeconfig_variable.random_id: Creation complete after 1s [id=projects/dev-ict-sp-6ff6/configs/service-project-config/variables/random-id]
google_runtimeconfig_variable.label_createdon: Creation complete after 1s [id=projects/dev-ict-sp-6ff6/configs/service-project-config/variables/label-createdon]
google_runtimeconfig_variable.resource_labels: Creation complete after 1s [id=projects/dev-ict-sp-6ff6/configs/service-project-config/variables/resource-labels]
google_runtimeconfig_variable.project_id: Creation complete after 1s [id=projects/dev-ict-sp-6ff6/configs/service-project-config/variables/project-id]
time_sleep.wait_api_activation: Still creating... [10s elapsed]
time_sleep.wait_api_activation: Still creating... [20s elapsed]
time_sleep.wait_api_activation: Creation complete after 25s [id=2022-08-03T22:45:01Z]
data.google_compute_global_address.private_ip_address: Reading...
google_compute_shared_vpc_service_project.this: Creating...
data.google_compute_global_address.private_ip_address: Read complete after 1s
google_compute_shared_vpc_service_project.this: Still creating... [10s elapsed]
google_compute_shared_vpc_service_project.this: Creation complete after 12s [id=dev-ict-hp-6ff6/dev-ict-sp-6ff6]
Releasing state lock. This may take a few moments...

Apply complete! Resources: 18 added, 0 changed, 0 destroyed.

```
Outputs:

project_id = "dev-ict-sp-6ff6"
random_id = "6ff6"

## Implementation of pub-sub module

This module allows managing a single Pub/Sub topic, including multiple subscriptions and IAM bindings at the topic and subscriptions levels.

Change the module and variable values required for the module in the `main.tf` file. the following steps:

1. Consuming pub-sub module in the `main.tf` file.

```
module "pub-sub" {
  source     = "../modules/pub-sub"
  project_id = var.service_project_id
  name = var.name

#iam roles

  iam = {
    "roles/pubsub.subscriber"  =  var.members
  }

#Subscriptions are defined with the `subscriptions` variable, allowing optional configuration of per-subscription defaults.

  subscriptions = {
    test-pull = null
    test-pull-override = {
      labels = { test = "override" }
      options = {
        ack_deadline_seconds       = var.ack_deadline_seconds
        message_retention_duration = var.message_retention_duration
        retain_acked_messages      = var.retain_acked_messages
        expiration_policy_ttl      = var.expiration_policy_ttl
        filter                     = var.filter
      }
    }
  }

   push_configs = {
    test-push = {
      endpoint   = var.endpoint
      attributes = var.attributes
      oidc_token = null
    }
  }

  subscription_iam = {
    test-pull = {
      "roles/pubsub.subscriber" = var.members
    }
  }
}

```

2. Create a `pub-sub.tfvars` file containing the variables defined in `variables.tf` and their values.

```
credentials_file  = "C:/Users/raiad/Desktop/service_key/Secret.json"
billing_account   = "00615A-599C36-C77931"
folder_id         = "folders/235483870619"
project_base_name = "ict-sp"
environment       = "dev"
assign_unique_id  = true
unique_id         = "6ff6"
host_project_id   = "dev-ict-hp-6ff6"
members = [ "serviceAccount:terraform@dematic-ict-registry.iam.gserviceaccount.com" ]
name = "my-topic"
service_project_id = "dev-ict-sp-6ff6"
ack_deadline_seconds = 10
retain_acked_messages = true
endpoint = "https://example.com/foo"

```
3. Initialize the Terraform provider.

```bash
$ terraform init -backend-config=credentials=C:/Users/raiad/Desktop/service_key/Secret.json -backend-config=prefix=root/dev-service-project/pub-sub
```

4. Validate the Terraform plan.

```bash
$ terraform plan -var-file="pub-sub.tfvars" -out dev-service-pub-sub.tfplan -target=module.pub-sub
```

5. Implement the Terraform plan.

```bash
$ terraform apply -auto-approve dev-service-pub-sub.tfplan
```

Example output:
```
module.pub-sub.google_pubsub_topic.default: Creating...
module.pub-sub.google_pubsub_topic.default: Creation complete after 6s [id=projects/dev-ict-sp-6ff6/topics/my-topic]
module.pub-sub.google_pubsub_subscription.default["test-pull"]: Creating...
module.pub-sub.google_pubsub_topic_iam_binding.default["roles/pubsub.subscriber"]: Creating...
module.pub-sub.google_pubsub_subscription.default["test-pull-override"]: Creating...
module.pub-sub.google_pubsub_subscription.default["test-pull-override"]: Creation complete after 2s [id=projects/dev-ict-sp-6ff6/subscriptions/test-pull-override]
module.pub-sub.google_pubsub_subscription.default["test-pull"]: Creation complete after 3s [id=projects/dev-ict-sp-6ff6/subscriptions/test-pull]
module.pub-sub.google_pubsub_subscription_iam_binding.default["test-pull.roles/pubsub.subscriber"]: Creating...
module.pub-sub.google_pubsub_topic_iam_binding.default["roles/pubsub.subscriber"]: Creation complete after 6s [id=projects/dev-ict-sp-6ff6/topics/my-topic/roles/pubsub.subscriber]
module.pub-sub.google_pubsub_subscription_iam_binding.default["test-pull.roles/pubsub.subscriber"]: Creation complete after 6s [id=projects/dev-ict-sp-6ff6/subscriptions/test-pull/roles/pubsub.subscriber]

Apply complete! Resources: 5 added, 0 changed, 0 destroyed.

```

<!--- BEGIN_TF_DOCS --->

## Providers

| Name   | Version  |
| ------ | -------- |
| google | >= 4.5.0 |
| time   | >= 0.7.0 |
| random | >= 3.1.0 |

## Inputs

| Name                      | Description                                                                                            | Type           | Default       | Required |
| ------------------------- | ------------------------------------------------------------------------------------------------------ | -------------- | ------------- | :------: |
| credentials_file          | Service account credentials file                                                                       | `string`       | n/a           |   yes    |
| host_project_id           | The host project identifier                                                                            | `string`       | n/a           |    no    |
| project_base_name         | The base name of the project.                                                                          | `string`       | n/a           |   yes    |
| environment               | The environment abbreviation to be used in resource naming deployment                                  | `string`       | n/a           |   yes    |
| billing_account           | The billing account number used for the project                                                        | `string`       | n/a           |   yes    |
| folder_id                 | The folder identifier where the project will reside                                                    | `string`       | n/a           |   yes    |
| labels                    | Labels to be applied to resources (inclusive)                                                          | `map(string)`  | n/a           |    no    |
| runtime_labels            | Additional labels that can be supplied in addition to what CI has defined in a tfvars file (inclusive) | `map(string)`  | {}            |    no    |
| unique_id                 | Random identifier used with resource naming                                                            | `string`       | null          |    no    |
| assign_unique_id          | If true, use a random identifier when naming resources                                                 | `bool`         | `true`        |    no    |
| services                  | List of services that need to be enabled for the project                                               | `list(string)` | []            |    no    |
| vpc_connect_ip_cidr_range | The IP CIDR range for the VPC connector                                                                | `string`       | `10.8.0.0/28` |    no    |

### inputs for pub-sub module

| Name                      | Description                                                                                            | Type           | Default          | Required |
| ------------------------- | ------------------------------------------------------------------------------------------------------ | -------------- | -------------    | :------: |
| ack_deadline_seconds      | acknowledge deadline seconds                                                                           | `number`       | null             |   no     |
| attributes                | attributes in `push_config`                                                                            | `map(string)`  | null             |   no     |
| endpoint                  | endpoints for  push suscription                                                                        | `map(string)`  | {"key"} = "null" |   no     |
| expiration_policy_ttl     |                                                                                                        | `string`       | null             |   no     |
| filter                    |                                                                                                        | `string`       | null             |   yes    |
| members                   | Users/SAs to be given invoker access to the service.                                                   | `string`       | null             |   no     |
| message_retention_duration| Minimum duration to retain a message after it is published to the topic.                               | `string`       | null             |   no     |
| name                      | PubSub topic name.                                                                                     | `string`       | null             |   yes    |
| retain_acked_messages     | Retain Acknowledge message                                                                             | `bool`         | null             |   no     |
| service_project_id        | Project used for resources                                                                             | `string`       | null             |   yes    |

## Outputs

| Name                                    | Description                                                                             |
| --------------------------------------- | --------------------------------------------------------------------------------------- |
| random_id                               | Random identifier used with resource naming                                             |
| project_id                              | The service project identifier                                                          |

<!--- END_TF_DOCS --->
```
